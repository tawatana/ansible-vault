# ansible-vault
ansible-vault のデモプログラム  
書き方は以下リンクに従っています。  
https://docs.ansible.com/ansible/2.9_ja/user_guide/vault.html
https://docs.ansible.com/ansible/2.9_ja/user_guide/playbooks_best_practices.html#vault 
# How to USE
```
# 暗号化されたいないplaybookを実行
cd not_vault
ansible-playbook -i inventory playbook.yml 

# 暗号化されたplaybookを実行
cd vault 
ansible-playbook -i inventory playbook.yml --vault-id vault_pass
```